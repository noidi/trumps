eq(pos,expected,actual,epsilon)
 new delta
 set delta=expected-actual
 set:delta<0 delta=-delta
 if delta<=$get(epsilon,0) do
 . write "."
 else  do
 . write !,pos,": ",actual," (expected ",expected,")",!!
 . halt
 quit
