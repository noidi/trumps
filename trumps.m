 set spheresEnabled=1
 set planeEnabled=1
 set illuminationEnabled=1
 set diffuseEnabled=1
 set specularEnabled=1
 set bumpMappingEnabled=1
 set fogEnabled=1
 set checkersEnabled=1
 ; set resX=16,resY=12
 ; set resX=160,resY=120
 ; set resX=320,resY=240
 ; set resX=640,resY=480
 set resX=1024,resY=768

 do create^vec(0,0,0,.origin)

 set bumpedWhiteMaterial("texture")="solid"
 set bumpedWhiteMaterial("color","r")=1
 set bumpedWhiteMaterial("color","g")=1
 set bumpedWhiteMaterial("color","b")=1
 set bumpedWhiteMaterial("bumped")=1

 set checkeredMaterial("texture")="checkered"
 set checkeredMaterial("bumped")=0

 do create^vec(0,-1,0,.planeNormal)
 set planeD=-1

 do create^vec(-1,-1,3,.spherePos)
 merge spheres(0,"pos")=spherePos
 set spheres(0,"r")=1

 do create^vec(0,1.2,5,.spherePos)
 merge spheres(1,"pos")=spherePos
 set spheres(1,"r")=1

 do create^vec(8,0,15,.spherePos)
 merge spheres(2,"pos")=spherePos
 set spheres(2,"r")=1

 do create^vec(-1,1,8,.spherePos)
 merge spheres(3,"pos")=spherePos
 set spheres(3,"r")=1

 do create^vec(4,-2,12,.spherePos)
 merge spheres(4,"pos")=spherePos
 set spheres(4,"r")=1
 kill spherePos

 do create^vec(-2,-2,1,.lightPos)
 merge lights(0,"pos")=lightPos
 set lights(0,"color","r")=1.2
 set lights(0,"color","g")=1.0
 set lights(0,"color","b")=0.8

 do create^vec(2,2,4,.lightPos)
 merge lights(1,"pos")=lightPos
 set lights(1,"color","r")=0.1
 set lights(1,"color","g")=0.2
 set lights(1,"color","b")=0.3

 do create^vec(0,-5,14,.lightPos)
 merge lights(2,"pos")=lightPos
 set lights(2,"color","r")=1.0
 set lights(2,"color","g")=1.0
 set lights(2,"color","b")=1.0
 kill lightPos

 new fb,window,renderer,texture
 do create^fb(resX,resY,.fb)
 do init^sdl($$InitVideo^sdl)
 set window=$$createWindow^sdl("TRUMPS",$$WindowPosUndefined^sdl,$$WindowPosUndefined^sdl,1024,768,$$WindowShown^sdl)
 set renderer=$$createRenderer^sdl(window,-1,0)
 set texture=$$createTexture^sdl(renderer,$$PixelformatARGB8888^sdl,$$TextureAccessStreaming^sdl,fb("width"),fb("height"))

 new aspect,viewPlaneWidth,viewPlaneHeight
 set aspect=fb("width")/fb("height")
 set viewPlaneHeight=1
 set viewPlaneWidth=aspect*viewPlaneHeight

 new viewPlaneLeft,viewPlaneRight,viewPlaneTop,viewPlaneBottom
 set viewPlaneLeft=-(viewPlaneWidth/2)
 set viewPlaneRight=+(viewPlaneWidth/2)
 set viewPlaneTop=-(viewPlaneHeight/2)
 set viewPlaneBottom=+(viewPlaneHeight/2)

 new viewPlaneDistance
 set viewPlaneDistance=1

 new pixelWidth,pixelHeight
 set pixelWidth=viewPlaneWidth/fb("width")
 set pixelHeight=viewPlaneHeight/fb("height")

 new pixelPos,ray
 do create^vec(viewPlaneLeft,viewPlaneTop,viewPlaneDistance,.pixelPos)

 do clear^fb(.fb)
 for y=0:1:fb("height")-1 do
 . do sset^vec(.pixelPos,viewPlaneLeft)
 . for x=0:1:fb("width")-1 do
 . . merge ray=pixelPos
 . . do normalize^vec(.ray)
 . . do render(.fb,x,y,.ray)
 . . do sadd^vec(.pixelPos,pixelWidth)
 . do sadd^vec(.pixelPos,,pixelHeight)
 . do updateTexture^sdl(texture,0,fb("pixels"),4*fb("width"))
 . do renderCopy^sdl(renderer,texture,0,0)
 . do renderPresent^sdl(renderer)

 read foo
 do quit^sdl
 quit

intersectSphere(ray,result)
 new k,spherePos,sphereR,a,b,c,solution,t,pos,normal
 set k=""  for  set k=$order(spheres(k))  quit:k=""!$get(result("found"),0)  do
 . merge spherePos=spheres(k,"pos")
 . set sphereR=spheres(k,"r")
 . set a=(ray("i")**2)+(ray("j")**2)+(ray("k")**2)
 . set b=2*((ray("i")*(-spherePos("i")))+(ray("j")*(-spherePos("j")))+(ray("k")*(-spherePos("k"))))
 . set c=(spherePos("i")**2)+(spherePos("j")**2)+(spherePos("k")**2)-(sphereR**2)
 . do quadratic(a,b,c,.solution)
 . if solution("rootCount")=0 do
 . . set result("found")=0
 . else  do
 . . if solution("t0")>100!solution("t1")<1 do
 . . . set result("found")=0
 . . else  do
 . . . if solution("t0")>=1 do
 . . . . set t=solution("t0")
 . . . else  do
 . . . . set t=solution("t1")
 . . . if t>100 do
 . . . . set result("found")=0
 . . . else  do
 . . . . set result("found")=1
 . . . . set result("t")=t
 . . . . merge pos=ray
 . . . . do smul^vec(.pos,t)
 . . . . merge result("pos")=pos
 . . . . merge normal=pos
 . . . . do sub^vec(.normal,.spherePos)
 . . . . do normalize^vec(.normal)
 . . . . merge result("normal")=normal
 . . . . set result("material")="bumpedWhiteMaterial"
 quit

intersectPlane(ray,result)
 new d,t,pos
 set d=$$dot^vec(.planeNormal,.ray)
 if d'=0 do
 . set t=($$dot^vec(.planeNormal,.origin)+planeD)/d
 . if (t>0)&(t<100) do
 . . set result("found")=1
 . . set result("t")=t
 . . merge pos=ray
 . . do smul^vec(.pos,t)
 . . merge result("pos")=pos
 . . merge result("normal")=planeNormal
 . . set result("material")="checkeredMaterial"
 . else  do
 . . set result("found")=0
 else  do
 . set result("found")=0
 quit

intersect(ray,result)
 if spheresEnabled do intersectSphere(.ray,.result)
 if spheresEnabled quit:result("found")
 if planeEnabled do intersectPlane(.ray,.result)
 quit

quadratic(a,b,c,result)
 new discr,rootDiscr,q,tmp
 set discr=$$discriminant(a,b,c)
 if discr<=0 do
 . set result("rootCount")=0
 else  do
 . set rootDiscr=discr**.5
 . if b<0 do
 . . set q=-.5*(b-rootDiscr)
 . else  do
 . . set q=-.5*(b+rootDiscr)
 . set result("rootCount")=2
 . set result("t0")=q/a
 . set result("t1")=c/q
 . if result("t0")>result("t1") do
 . . set tmp=result("t0")
 . . set result("t0")=result("t1")
 . . set result("t1")=tmp
 quit

discriminant(a,b,c)
 quit (b**2)-(4*a*c)

illumination(pos,normal,result)
 new k,l,v,r,rSub,diffuse,specular
 set result("r")=0
 set result("g")=0
 set result("b")=0
 set k=""  for  set k=$order(lights(k))  quit:k=""  do
 . merge l=lights(k,"pos")
 . do sub^vec(.l,.pos)
 . do normalize^vec(.l)
 . set diffuse=$$dot^vec(.l,.normal)
 . set:diffuse<0 diffuse=0
 . merge v=pos
 . do normalize^vec(.v)
 . merge rSub=normal
 . do smul^vec(.rSub,2*$$dot^vec(.l,.normal))
 . merge r=l
 . do sub^vec(.r,.rSub)
 . set specular=$$dot^vec(.v,.r)**70
 . set:specular<0 specular=0
 . set:specular>1 specular=1
 . if diffuseEnabled do
 . . set result("r")=result("r")+(diffuse*lights(k,"color","r"))
 . . set result("g")=result("g")+(diffuse*lights(k,"color","g"))
 . . set result("b")=result("b")+(diffuse*lights(k,"color","b"))
 . if specularEnabled do
 . . set result("r")=result("r")+(specular*lights(k,"color","r"))
 . . set result("g")=result("g")+(specular*lights(k,"color","g"))
 . . set result("b")=result("b")+(specular*lights(k,"color","b"))
 quit

xor(a,b)
 quit (a&'b)!('a&b)

checker(pos,result)
 new si,sk
 set si=(pos("i")#2)>1
 set sk=(pos("k")#2)>1
 set result("r")=$$xor(si,sk)
 set result("g")=$$xor(si,sk)
 set result("b")=$$xor(si,sk)
 quit

render(fb,x,y,ray)
 new intersection,pos,material,bumpedNormal,i,f,texel,r,g,b
 do intersect(.ray,.intersection)
 if intersection("found") do
 . merge material=@intersection("material")
 . merge pos=intersection("pos")
 . merge bumpedNormal=intersection("normal")
 . if bumpMappingEnabled&(material("bumped")) do
 . . do sadd^vec(.bumpedNormal,0.2*$$sin^sdl(37.931*pos("i")),0.2*$$sin^sdl(36.3*pos("j")),0.2*$$sin^sdl(34.32*pos("k")))
 . . do normalize^vec(.bumpedNormal)
 . if illuminationEnabled do
 . . do illumination(.pos,.bumpedNormal,.i)
 . else  do
 . . set i("r")=1
 . . set i("g")=1
 . . set i("b")=1
 . if fogEnabled do
 . . set f=1-(intersection("t")/17)
 . else  do
 . . set f=1
 . set:f<0 f=0
 . if material("texture")="solid" do
 . . merge texel=material("color")
 . else  if checkersEnabled do
 . . do checker(.pos,.texel)
 . else  do
 . . set texel("r")=1
 . . set texel("g")=1
 . . set texel("b")=1
 . set r=f*i("r")*texel("r")
 . set g=f*i("g")*texel("g")
 . set b=f*i("b")*texel("b")
 . set:r>1 r=1
 . set:g>1 g=1
 . set:b>1 b=1
 . do put^fb(.fb,x,y,$fn(255*r,"",0),$fn(255*g,"",0),$fn(255*b,"",0))
 quit
