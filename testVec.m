create
 new v
 do create^vec(1.23,4.56,7.89,.v)
 do eq^assert($zpos,1.23,v("i"))
 do eq^assert($zpos,4.56,v("j"))
 do eq^assert($zpos,7.89,v("k"))
 kill v

normalize
 new v
 do create^vec(1.23,4.56,7.89,.v)
 do normalize^vec(.v)
 do eq^assert($zpos,0.13376,v("i"),0.00001)
 do eq^assert($zpos,0.495891,v("j"),0.00001)
 do eq^assert($zpos,0.858021,v("k"),0.00001)
 kill v

smul
 new v
 do create^vec(1.23,4.56,7.89,.v)
 do smul^vec(.v,3)
 do eq^assert($zpos,3.69,v("i"))
 do eq^assert($zpos,13.68,v("j"))
 do eq^assert($zpos,23.67,v("k"))
 kill v

sadd
 new v
 do create^vec(1.23,4.56,7.89,.v)
 do sadd^vec(.v,2.34,5.67,8.90)
 do eq^assert($zpos,3.57,v("i"))
 do eq^assert($zpos,10.23,v("j"))
 do eq^assert($zpos,16.79,v("k"))
 kill v

saddMissingParams
 new v
 do create^vec(1.23,4.56,7.89,.v)
 do sadd^vec(.v)
 do eq^assert($zpos,1.23,v("i"))
 do eq^assert($zpos,4.56,v("j"))
 do eq^assert($zpos,7.89,v("k"))
 kill v

dot
 new v1,v2
 do create^vec(12,20,8,.v1)
 do create^vec(16,-5,2,.v2)
 do eq^assert($zpos,108,$$dot^vec(.v1,.v2))
 kill v1,v2

str
 new v
 do create^vec(1.23,4.56,7.89,.v)
 do eq^assert($zpos,1,"(1.23,4.56,7.89)"=$$str^vec(.v))
 kill v

sset
 new v
 do create^vec(1.23,4.56,7.89,.v)
 do sset^vec(.v,2.34,5.67,8.90)
 do eq^assert($zpos,2.34,v("i"))
 do eq^assert($zpos,5.67,v("j"))
 do eq^assert($zpos,8.90,v("k"))
 kill v

ssetMissingParams
 new v
 do create^vec(1.23,4.56,7.89,.v)
 do sset^vec(.v)
 do eq^assert($zpos,1.23,v("i"))
 do eq^assert($zpos,4.56,v("j"))
 do eq^assert($zpos,7.89,v("k"))
 kill v

sub
 new v1,v2
 do create^vec(1.23,4.56,7.89,.v1)
 do create^vec(2.34,5.68,8.92,.v2)
 do sub^vec(.v2,.v1)
 do eq^assert($zpos,1.11,v2("i"))
 do eq^assert($zpos,1.12,v2("j"))
 do eq^assert($zpos,1.03,v2("k"))
 kill v1,v2
