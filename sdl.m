InitVideo() quit $$FUNC^%HD("20")
WindowShown() quit $$FUNC^%HD("4")
WindowPosUndefined() quit $$FUNC^%HD("1FFF0000")
PixelformatRGB888() quit $$FUNC^%HD("16161804")
PixelformatARGB8888() quit $$FUNC^%HD("16362004")
TextureAccessStreaming() quit $$FUNC^%HD("1")

init(flags)
 new result
 do &sdl.init(flags,.result)
 if result'=0 do fail("init")
 quit

createWindow(title,x,y,w,h,flags)
 new result
 do &sdl.createWindow(title,x,y,w,h,flags,.result)
 if result=0 do fail("createWindow")
 quit result

delay(ms)
 do &sdl.delay(ms)
 quit

quit()
 do &sdl.quit
 quit

createRenderer(window,index,flags)
 new result
 do &sdl.createRenderer(window,index,flags,.result)
 if result=0 do fail("createRenderer")
 quit result

createTexture(renderer,format,access,w,h)
 new result
 do &sdl.createTexture(renderer,format,access,w,h,.result)
 if result=0 do fail("createTexture")
 quit result

setRenderDrawColor(renderer,r,g,b,a)
 new result
 do &sdl.setRenderDrawColor(renderer,r,g,b,a,.result)
 if result'=0 do fail("setRenderDrawColor")
 quit

renderClear(renderer)
 new result
 do &sdl.renderClear(renderer,.result)
 if result'=0 do fail("renderClear")
 quit

renderPresent(renderer)
 do &sdl.renderPresent(renderer)
 quit

updateTexture(texture,rect,pixels,pitch)
 new result
 do &sdl.updateTexture(texture,rect,pixels,pitch,.result)
 if result'=0 do fail("updateTexture")
 quit

renderCopy(renderer,texture,srcrect,dstrect)
 new result
 do &sdl.renderCopy(renderer,texture,srcrect,dstrect,.result)
 if result'=0 do fail("renderCopy")
 quit

malloc(size)
 new result
 do &sdl.malloc(size,.result)
 quit result

free(ptr)
 do &sdl.free(ptr)
 quit

memset(s,c,n)
 do &sdl.memset(s,c,n)
 quit

putUint32(ptr,x)
 do &sdl.putUint32(ptr,x)
 quit

fail(routineName)
 write routineName,"^sdl failed!",!
 do &sdl.quit
 halt

sin(x)
 new result
 do &sdl.sin(x,.result)
 quit result
