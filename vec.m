create(i,j,k,result)
 set result("i")=i
 set result("j")=j
 set result("k")=k
 quit

normalize(v)
 new length
 set length=(v("i")*v("i"))+(v("j")*v("j"))+(v("k")*v("k"))**.5
 set v("i")=v("i")/length
 set v("j")=v("j")/length
 set v("k")=v("k")/length
 quit

smul(v,s)
 set v("i")=s*v("i")
 set v("j")=s*v("j")
 set v("k")=s*v("k")
 quit

sadd(v,di,dj,dk)
 set v("i")=v("i")+$get(di,0)
 set v("j")=v("j")+$get(dj,0)
 set v("k")=v("k")+$get(dk,0)
 quit

dot(v1,v2)
 quit (v1("i")*v2("i"))+(v1("j")*v2("j"))+(v1("k")*v2("k"))

str(v)
 quit "("_v("i")_","_v("j")_","_v("k")_")"

sset(v,i,j,k)
 set v("i")=$get(i,v("i"))
 set v("j")=$get(j,v("j"))
 set v("k")=$get(k,v("k"))
 quit

sub(v1,v2)
 set v1("i")=v1("i")-v2("i")
 set v1("j")=v1("j")-v2("j")
 set v1("k")=v1("k")-v2("k")
 quit
