#include <stdio.h>
#include <SDL2/SDL.h>
#include <gtmxc_types.h>

void m_SDL_Init(int _, gtm_ulong_t flags, gtm_long_t* result) {
    *result = SDL_Init(flags);
}

void m_SDL_CreateWindow(int _, gtm_char_t* title, gtm_ulong_t x, gtm_ulong_t y, gtm_ulong_t w, gtm_ulong_t h, gtm_ulong_t flags, gtm_long_t* result) {
    *result = (long)SDL_CreateWindow(title, x, y, w, h, flags);
}

void m_SDL_Delay(int _, gtm_ulong_t ms) {
    SDL_Delay(ms);
}

void m_SDL_Quit(int _) {
    SDL_Quit();
}

void m_SDL_CreateRenderer(int _, gtm_long_t window, gtm_long_t index, gtm_ulong_t flags, gtm_long_t* result) {
    *result = (long)SDL_CreateRenderer((SDL_Window*)window, index, flags);
}

void m_SDL_CreateTexture(int _, gtm_long_t renderer, gtm_ulong_t format, gtm_long_t access, gtm_long_t w, gtm_long_t h, gtm_long_t* result) {
    *result = (long)SDL_CreateTexture((SDL_Renderer*)renderer, format, access, w, h);
}

void m_SDL_SetRenderDrawColor(int _, gtm_long_t renderer, gtm_long_t r, gtm_long_t g, gtm_long_t b, gtm_long_t a, gtm_long_t* result) {
    *result = SDL_SetRenderDrawColor((SDL_Renderer*)renderer, r, g, b, a);
}

void m_SDL_RenderClear(int _, gtm_long_t renderer, gtm_long_t* result) {
    *result = SDL_RenderClear((SDL_Renderer*)renderer);
}

void m_SDL_RenderPresent(int _, gtm_long_t renderer) {
    SDL_RenderPresent((SDL_Renderer*)renderer);
}

void m_SDL_UpdateTexture(int _, gtm_long_t texture, gtm_long_t rect, gtm_long_t pixels, gtm_long_t pitch, gtm_long_t* result) {
    *result = SDL_UpdateTexture((SDL_Texture*)texture, (const SDL_Rect*)rect, (void*)pixels, pitch);
}

void m_SDL_RenderCopy(int _, gtm_long_t renderer, gtm_long_t texture, gtm_long_t srcrect, gtm_long_t dstrect, gtm_long_t* result) {
    *result = SDL_RenderCopy((SDL_Renderer*)renderer, (SDL_Texture*)texture, (const SDL_Rect*)srcrect, (const SDL_Rect*)dstrect);
}

void m_malloc(int _, gtm_long_t size, gtm_long_t* result) {
    *result = (long)malloc(size);
}

void m_free(int _, gtm_long_t ptr) {
    free((void*)ptr);
}

void m_memset(int _, gtm_long_t s, gtm_long_t c, gtm_long_t n) {
    memset((void*)s, (int)c, n);
}

void m_putUint32(int _, gtm_long_t ptr, gtm_long_t x) {
    *(uint32_t*)ptr = (uint32_t)x;
}

void m_sin(int _, gtm_double_t* x, gtm_double_t* result) {
    *result = sin(*x);
}
