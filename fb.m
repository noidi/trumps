create(width,height,result)
 set result("pixels")=$$malloc^sdl(width*height*4)
 if result("pixels")=0 do
 . write "malloc failed!",!
 . halt
 set result("width")=width
 set result("height")=height
 quit

clear(fb)
 do memset^sdl(fb("pixels"),0,fb("width")*fb("height")*4)
 quit

put(fb,x,y,r,g,b)
 do putUint32^sdl(fb("pixels")+((y*fb("width")+x)*4),(256*256*r)+(256*g)+b)
 quit
